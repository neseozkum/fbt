﻿using System;
using System.Collections;
using System.Collections.Generic;


using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class StartMenu : MonoBehaviour
{
    public dreamloLeaderBoard db;
    public Canvas PnlC;
    public Button btncopen,btncclose;
    
    // Use this for initialization
    void Start()
    {
        Button btn = btncopen.GetComponent<Button>();
        Button btn2 = btncclose.GetComponent<Button>();
        btn.onClick.AddListener(BtnCOpen);
        btn2.onClick.AddListener(BtnClose);
        int firstlogin= PlayerPrefs.GetInt("firslogin");
if(firstlogin==0){
     db.LoadScore(SystemInfo.deviceUniqueIdentifier);
      PlayerPrefs.SetInt("firslogin",1);
}

    }

    // Update is called once per frame
    void Update()
    {
    }
  

    public void LoadScene(string scene)
    {
        Debug.Log("button clicked");
        SceneManager.LoadScene(scene);
    }

    public void BtnCOpen()
    {
        PnlC.enabled = true;
    }
    public void BtnClose()
    {
        PnlC.enabled = false;
    }
}