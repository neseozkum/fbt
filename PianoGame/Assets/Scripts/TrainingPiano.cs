﻿using System.Collections;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class TrainingPiano : MonoBehaviour
{

    private const int Transpose = -4;

    public void OnPlay(string note)
    {
        var noteValue = 0f;
        switch (note)
        {
            case "C":
                noteValue = 0;
                break;
            case "CD":
                noteValue = 1;
                break;
            case "D":
                noteValue = 2;
                break;
            case "DE":
                noteValue = 3;
                break;
            case "E":
                noteValue = 4;
                break;
            case "F":
                noteValue = 5;
                break;
            case "FG":
                noteValue = 6;
                break;
            case "G":
                noteValue = 7;
                break;
            case "GA":
                noteValue = 8;
                break;
            case "A":
                noteValue = 9;
                break;
            case "AB":
                noteValue = 10;
                break;
            case "B":
                noteValue = 11;
                break;
            case "C2":
                noteValue = 12;
                break;
        }

        if (!(noteValue >= 0)) return;

        var n = Mathf.Pow(2, (noteValue + Transpose) / 12f);
   
        GetComponent<AudioSource>().pitch = n;
        GetComponent<AudioSource>().Play();
    }

    public void LoadScene(string scene)
    {
        Debug.Log("button clicked");
        SceneManager.LoadScene(scene);
    }

    void Start()
    {
               Screen.orientation = ScreenOrientation.LandscapeLeft;

    }
}