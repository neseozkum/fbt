﻿using System.Collections;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayPiano : MonoBehaviour
{
    public Dropdown DdlNoteSize;
    public Canvas PnlOk;
    public Canvas PnlError;

    private int _noteSize;
    private ArrayList _missionNotes;
    private ArrayList _playedNotes;
    private const int Transpose = -4;

    public void SelectedNoteSize()
    {
        Setup();
    }

    public void OnPlay(string note)
    {
        var noteValue = 0f;
        switch (note)
        {
            case "C":
                noteValue = 0;
                break;
            case "CD":
                noteValue = 1;
                break;
            case "D":
                noteValue = 2;
                break;
            case "DE":
                noteValue = 3;
                break;
            case "E":
                noteValue = 4;
                break;
            case "F":
                noteValue = 5;
                break;
            case "FG":
                noteValue = 6;
                break;
            case "G":
                noteValue = 7;
                break;
            case "GA":
                noteValue = 8;
                break;
            case "A":
                noteValue = 9;
                break;
            case "AB":
                noteValue = 10;
                break;
            case "B":
                noteValue = 11;
                break;
            case "C2":
                noteValue = 12;
                break;
        }

        if (!(noteValue >= 0)) return;

        var n = Mathf.Pow(2, (noteValue + Transpose) / 12f);
        Debug.Log(string.Format("note: {0}", n));
        GetComponent<AudioSource>().pitch = n;
        GetComponent<AudioSource>().Play();
        _playedNotes.Add(n);
        StartCoroutine(CheckNotes());

    }

    private IEnumerator CheckNotes()
    {
        Debug.Log("_missionNotes.Count: " + _missionNotes.Count);
        Debug.Log("_playedNotes.Count: " + _playedNotes.Count);
        if (_missionNotes.Count == _playedNotes.Count)
        {
            Debug.Log("esit");
            if (_missionNotes.ToArray().SequenceEqual(_playedNotes.ToArray()))
            {
                _missionNotes.Clear();
                _playedNotes.Clear();

                yield return new WaitForSeconds(2);
                PnlOk.enabled = true;
                yield return new WaitForSeconds(2);
                PnlOk.enabled = false;
                yield return new WaitForSeconds(1);
                PlayNotes();

                Debug.Log("aynı");
            }
            else
            {
                yield return new WaitForSeconds(2);
                PnlError.enabled = true;
                Debug.Log("aynı değil");
            }

            _playedNotes.Clear();
        }

    }

    private AudioSource _sound;

    public void PlayButtonClicked()
    {    
        StartCoroutine(PlayScale());
    }

    void Start()
    {
		        Screen.orientation = ScreenOrientation.LandscapeLeft;
        _sound = GetComponent<AudioSource>();
        Setup();
    }

    private void Setup()
    {
        _noteSize = DdlNoteSize.value + 2;     
        _missionNotes = new ArrayList();
        _playedNotes = new ArrayList();
        PlayNotes();
    }

    private void PlayNotes()
    {
        _sound = GetComponent<AudioSource>();
        FillArrayNotes();
        StartCoroutine(PlayScale());
    }

    private IEnumerator PlayScale()
    {
        foreach (var playNote in _missionNotes)
        {
            Debug.Log("pn:" + (float) playNote);
            GetComponent<AudioSource>().pitch = (float) playNote;
            GetComponent<AudioSource>().Play();
            yield return new WaitForSeconds(2f);
        }
    }

    private void FillArrayNotes()
    {
        int rndNotes;
        for (var i = 0; i < _noteSize; i++)
        {
            rndNotes = Random.Range(1, 12);
           
            _missionNotes.Add(Mathf.Pow(2, (rndNotes + Transpose) / 12f));
        }
    }

    public void TryAgain()
    {
        PnlError.enabled = false;
        PlayButtonClicked();

    }

    public void Continue()
    {
        StartCoroutine(x());
    }

    private IEnumerator x()
    {
        PnlError.enabled = false;
        yield return new WaitForSeconds(2);
        Setup();
    }
    public void LoadScene(string scene)
    {
        Debug.Log("button clicked");
        SceneManager.LoadScene(scene);
    }
}